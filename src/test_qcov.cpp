#define QCOV
#include <qcov.hpp>

#include <string>
#include <iostream>

void fun1();
std::string fun2(const int arg1);
void fun3();

int main(int argc, char** argv)
{$q
    fun1();
    std::cout << fun2(0) << "\n";
    std::cout << fun2(2) << "\n";

    for(size_t i = 0; i< 100; i++)
        fun3();

	$q_simple_show
	$q_clear
    return 0;
}

void fun1()
{$q
    std::cout << "fun1\n";
}

std::string fun2(const int arg1)
{$q
    std::cout << "fun2\n";
   if(arg1 < 1)
   {$qb
       std::cout << "branch 1\n";
   } 
   else
   {$qb
       std::cout << "branch 2\n";
   }
   if(arg1 < 1)
   {
       return "ent1";
   } 
   else
   {
       return "ent2";
   }
}

void fun3()
{$q
    std::cout << "fun3\n";
}