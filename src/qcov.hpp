#include <unordered_map>
#include <atomic>
#include <string>
#include <iostream>

#ifndef QCOV_HPP
#define QCOV_HPP
namespace qcov
{

void addEntrance(const std::string& key, const int& line);
void qcovClear();
void gcovSimpleShow();

#ifdef QCOV
#define $q qcov::addEntrance(std::string(__FILE__) + "-" + std::string(__func__), __LINE__);

#define $qb

#define $q_clear qcov::qcovClear();
#define $q_simple_show qcov::gcovSimpleShow();

#else
#define $q
#define $qb
#define $q_clear
#define $q_simple_show

#endif

	struct qcov_struct
	{
		std::atomic<int> ent;
		int line_num;
		std::unordered_map<int, std::atomic<int>> exits;
		std::unordered_map<int, std::atomic<int>> branch;

		qcov_struct(const int& t_line_num) : line_num(t_line_num)
		{
			ent.store(0);
		}
	};

	std::unordered_map<std::string, qcov_struct*> qcov_map;

	void addEntrance(const std::string& key, const int& line)
	{
		if (qcov_map.find(key) == qcov_map.end())
			qcov_map.insert(std::make_pair(key, new qcov_struct(line)));
		qcov_map[key]->ent++;
	}

	void qcovClear()
	{
		for (auto e : qcov_map)
			delete e.second;
	}

	void gcovSimpleShow()
	{
		std::cout << "\n\n";
		std::cout << "QCov\n";
		std::cout << "Simple Display\n\n";
		for (auto e : qcov_map)
		{
			auto s = e.first;
			size_t cut = s.find("-");
			std::cout << 
				"In file: " << s.substr(0, cut) << 
				":" << e.second->line_num << 
				"   function: " << s.substr(cut+1, s.size() - 1) << 
				"   entrance: " << e.second->ent.load() << 
				"\n";
		}
	}

}
#endif //QCOV
