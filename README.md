QCov
==============
Suite of tools for the development of your own application.

***

| branch        | pipeline      | coverage  |
| ------------- |:-------------:| :--------:|
| master | [![pipeline status](https://gitlab.com/zaba999/qcov/badges/master/pipeline.svg)](https://gitlab.com/zaba999/qcov/commits/master) | [![coverage report](https://gitlab.com/zaba999/qcov/badges/master/coverage.svg)](https://gitlab.com/zaba999/qcov/commits/master) |

***

[**Detail description**](#detail_description) | [**Getting Started**](#getting_started) | [**Build Linux**](#build_linux) | [**Build Windows**](#build_windows) |
[**Built With**](#built_with) | [**Contributing and questions**](#contributing_and_questions) | [**Versioning**](#versioning) | 
[**Authors**](#authors) | [**License**](#license)

***

## Detail description

## Getting Started

### Build Linux
Prepare files for build with Makefile:
```console
$ mkdir bin
$ cd bin
$ cmake -DCMAKE_BUILD_TYPE=Release -DQCOV_TEST=ON ../
$ make
```

### Build Windows
Build for Visual Studio 15 2017 compiler on Windows machine Win32:
```console
$ mkdir bin
$ cd bin
$ cmake -G "Visual Studio 15 2017" -DQCOV_TEST=ON ../
$ start TQ.sln
```

Build for Visual Studio 15 2017 compiler on Windows machine Win64:
```console
$ mkdir bin
$ cd bin
$ cmake -G "Visual Studio 15 2017 Win64" -DQCOV_TEST=ON ../
$ start TQ.sln
```

## Built With

* [CMake](https://cmake.org/) - Built tool

## Contributing and questions

For information on contributing, questions about software and language please contact via email:
* **Mikołaj Żabiński** [email](mzabinski94@gmail.com) 

## Versioning

[Changelog](https://gitlab.com/zaba999/qcov/blob/master/CHANGELOG)

## Authors

* **Mikołaj Żabiński** - *Initial work* - [zaba999](https://gitlab.com/zaba999)

See also the list of [contributors](https://gitlab.com/zaba999/qcov/graphs/master) who participated in this project.

## License

Copyright 2019 Mikołaj Żabiński

Full license text can be found under [LICENSE](https://gitlab.com/zaba999/qcov/blob/master/LICENSE)
